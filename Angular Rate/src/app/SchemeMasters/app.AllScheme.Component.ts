import { Component, OnInit, ViewChild } from '@angular/core';
import { SchemeService } from './Services/app.Scheme.Service';
import { SchemeMasterViewModel } from './Models/app.SchemeViewModel';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DialogService } from '../dialog-box/dialog-box.Service';
import { InformationService } from "../information/information.service";

@Component({
  templateUrl: './app.AllSchemeComponent.html',
  styleUrls: ['./app.AllSchemeComponent.css', '../Content/vendor/bootstrap/css/bootstrap.min.css', '../Content/vendor/font-awesome/css/font-awesome.min.css']

})

export class AllSchemeComponent implements OnInit {
  private _SchemeService;
  AllSchemeList: SchemeMasterViewModel[];
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'name', 'phone', 'address', 'email', 'sex', 'birthdate', 'fieldName', 'positionName', 'Edit', 'Delete'];
  dataSource: any;
  response: any;

  constructor(private location: Location, private _Route: Router, private schemeService: SchemeService,
    private dialogService: DialogService, public informationService: InformationService) {
    this._SchemeService = schemeService;
  }
  ngOnInit() {
    this.paginator._intl.itemsPerPageLabel = 'Số lượng nhân viên mỗi trang';
    this.reloadTable();
  }

  reloadTable() {
    this._SchemeService.GetAllScheme().subscribe(
      AllScheme => {
        this.AllSchemeList = AllScheme;
        this.dataSource = new MatTableDataSource(this.AllSchemeList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error => this.errorMessage = <any>error
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Delete(employeesId: any) {
    this.dialogService.confirm('Xác nhận xóa nhân viên', 'Bạn có chắc chắn muốn xóa nhân viên này ?')
      .then(async (result) => {
        if (result) {
          this.response = await this._SchemeService.DeleteScheme(employeesId).toPromise();
          if (this.response.status == "200") {
            this.informationService.openSnackBar('Xóa thành công nhân viên');
            this.reloadTable();
          }
          else {
            this.informationService.openSnackBar('Không xóa được nhân viên!');
            this._Route.navigate(['/Employees/All']);
          }
        }
      }
      )
  }
}