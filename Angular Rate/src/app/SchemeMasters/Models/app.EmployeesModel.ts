export class EmployeesModel 
{
    public ID: number=0;
    public Name: string="";
    public Phone: string="";
    public Address: string = "";
    public Email:string ="";
    public idSex: number = 0;
    public Birthdate: string = "";
    public idField: number = 0;
    public idPosition: number = 0;
    public Username: string ="";
    public Password: string ="";
    public AvaLink: string = "";
}