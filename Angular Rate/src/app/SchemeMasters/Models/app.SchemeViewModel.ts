export class SchemeMasterViewModel 
{
    public ID: number=0;
    public Name: string="";
    public Phone: string="";
    public Address: string = "";
    public Email:string ="";
    public Sex: string = "";
    public Birthdate: string = Date.toString();
    public FieldName: string ="";
    public PositionName: string="";
}