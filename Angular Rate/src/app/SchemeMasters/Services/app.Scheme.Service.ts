import { Injectable } from "@angular/core";
import { Observable, throwError, observable } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { EmployeesModel } from "../Models/app.EmployeesModel";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
import { SchemeMasterViewModel } from "../Models/app.SchemeViewModel";
import { SchemeDropdownModel } from "../Models/app.SchemeDropdownModel";
// import { environment } from "src/app/Shared/environment";
import { environment } from '../../../environments/environment';
import { DepartmentDropdownModel } from "../Models/app.DepartmentDropdownModel";
import { SexDropdownModel } from "../Models/app.SexDropdownModel";
import { FieldDropDownmodel } from "../Models/app.FieldDropDownModel";
import { InformationService } from "../../information/information.service";
import { DateFilter } from "../../AdminDashboard/Models/app.DateFilter";
import { DataSharingService } from "src/app/data-sharing-service";

@Injectable({
    providedIn: 'root'
})

export class SchemeService {
    private data: any;
    private apiUrl = environment.apiEndpoint + "/api/Employees";
    private apiUrlGetEmployees = environment.apiEndpoint + "/api/Employees/getAllById";
    private apiUpdateEmployees = environment.apiEndpoint + "/api/Employees/";
    private apiUrlDep = environment.apiEndpoint + "/api/Department/";
    private apiUrlTop5 = environment.apiEndpoint + "/api/Employees/topFive";
    private apiUrlField = environment.apiEndpoint + "/api/Field";
    private apiUploadImage = environment.apiEndpoint + "/api/Employees/UploadImage";
    token: any;
    username: any;

    constructor(private http: HttpClient, public informationService: InformationService, public dataSharingService : DataSharingService) {
        dataSharingService.Storage.subscribe(data => {
            try{
                this.data = JSON.parse(data.toString());
            }
            catch{
                
            }
        });
        this.token = this.data.token;
        this.username = this.data.username;
    }

    // Save Scheme
    public SaveScheme(EmployeesModel: EmployeesModel) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<any>(this.apiUrl, EmployeesModel, { headers: headers, observe: 'response' })
            .pipe(
                catchError(this.handleError)
            );

    }

    // Get All Scheme
    public GetAllScheme() {
        let dateFilter: DateFilter = { FromDate: '01-01-1970', ToDate: '01-01-2100' };
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<SchemeMasterViewModel[]>(this.apiUrlTop5, dateFilter, { headers: headers })
            .pipe(tap(data => data),
                catchError(this.handleError)
            );
    }

    // Get All Scheme List
    // public GetAllActiveSchemeList() {
    //     var apiUrl = environment.apiEndpoint + "/api/SchemeDropdown/";
    //     let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    //     headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    //     return this.http.get<SchemeDropdownModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
    //         catchError(this.handleError)
    //     );
    // }

    // Get All Scheme By ID
    public GetSchemeById(employeesId) {
        var editUrl = this.apiUrlGetEmployees + '/' + employeesId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SchemeMasterViewModel>(editUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }
    public GetAllDepartment() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SchemeMasterViewModel>(this.apiUrlDep, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    public GetAllField() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<FieldDropDownmodel[]>(this.apiUrlField, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    public GetAllSex() {
        var apiUrlSex = environment.apiEndpoint + "/api/Sex/";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SexDropdownModel>(apiUrlSex, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    public GetAllPosition() {
        var apiPosition = environment.apiEndpoint + "/api/Position/";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SchemeMasterViewModel>(apiPosition, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Update Scheme
    public UpdateScheme(EmployeesModel: EmployeesModel) {
        var putUrl = this.apiUpdateEmployees + EmployeesModel.ID;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.put<any>(putUrl, EmployeesModel, { headers: headers, observe: 'response' })
            .pipe(
                catchError(this.handleError)
            );
    }

    public DeleteScheme(employeesId) {
        var deleteUrl = this.apiUpdateEmployees + employeesId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.delete<any>(deleteUrl, { headers: headers, observe: 'response' })
            .pipe(
                catchError(this.handleError)
            );
    }

    public UploadAvatar(fd: FormData) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post(this.apiUploadImage, fd, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
            if (error.status === 400) {
                this.informationService.openSnackBar("Đã có lỗi xảy ra");
            }
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };



}
