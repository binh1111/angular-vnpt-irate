import { Component, OnInit } from '@angular/core';
import { SchemeService } from './Services/app.Scheme.Service';
import { EmployeesModel } from './Models/app.EmployeesModel';
import { DepartmentDropdownModel } from './Models/app.DepartmentDropdownModel';
import { SexDropdownModel } from './Models/app.SexDropdownModel';
import { PositionDropdownModel } from './Models/app.PositionDropdownModel';
import { Router, ActivatedRoute } from '@angular/router';
import { FieldDropDownmodel } from './Models/app.FieldDropDownModel';
import { element } from '@angular/core/src/render3';
import { HttpClientModule, HttpClient, HttpRequest } from '@angular/common/http';
import { InformationService } from "../information/information.service";
import { environment } from 'src/environments/environment';


@Component({
    templateUrl: './app.EditSchemeComponent.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class EditSchemeComponent implements OnInit {
    title = "Sửa thông tin người dùng";
    EmployeesForms: EmployeesModel = new EmployeesModel();
    DepartmentDrop: DepartmentDropdownModel[];
    PositionDrop: PositionDropdownModel[];
    SexDrop: SexDropdownModel[];
    FieldDrop: FieldDropDownmodel[];
    private _SchemeService;
    private responsedata: any;
    private employeesId: string;
    errorMessage: any;
    selectedFile: File = null;
    avatarElement: HTMLImageElement;

    constructor(private _Route: Router, private _routeParams: ActivatedRoute, private schemeService: SchemeService,
        private http: HttpClient, public informationService: InformationService) {
        this._SchemeService = schemeService;
    }

    ngOnInit() {
        this.employeesId = this._routeParams.snapshot.params['employeesId'];
        if (this.employeesId != null) {
            var data = this._SchemeService.GetSchemeById(this.employeesId).subscribe(
                Employees => {
                    this.EmployeesForms.ID = Employees.id;
                    this.EmployeesForms.Address = Employees.address;
                    this.EmployeesForms.Birthdate = Employees.birthdate.toString().substring(0, 10);
                    this.EmployeesForms.Email = Employees.email;
                    this.EmployeesForms.Name = Employees.name;
                    this.EmployeesForms.Password = Employees.password;
                    this.EmployeesForms.Phone = Employees.phone;
                    this.EmployeesForms.Username = Employees.username;
                    this.EmployeesForms.idField = Employees.idField;
                    this.EmployeesForms.idPosition = Employees.idPosition;
                    this.EmployeesForms.idSex = Employees.idSex;
                    this.EmployeesForms.AvaLink = environment.apiEndpoint + Employees.avaLink;
                },
                error => this.errorMessage = <any>error
            );
        }

        var dataDep = this._SchemeService.GetAllDepartment().subscribe(
            Department => {
                this.DepartmentDrop = new Array();
                Department.forEach(element => {
                    var Dep: DepartmentDropdownModel = new DepartmentDropdownModel();
                    Dep.ID = element.id;
                    Dep.Name = element.name;
                    this.DepartmentDrop.push(Dep);
                });
            },
            error => this.errorMessage = <any>error
        );

        var dataField = this._SchemeService.GetAllField().subscribe(
            Field => {
                this.FieldDrop = new Array();
                this.FieldDrop = Field;
            },
            error => this.errorMessage = <any>error
        );

        var dataPos = this._SchemeService.GetAllPosition().subscribe(
            Position => {
                this.PositionDrop = new Array();
                Position.forEach(element => {
                    var pos: PositionDropdownModel = new PositionDropdownModel();
                    pos.ID = element.id;
                    pos.Name = element.name;
                    this.PositionDrop.push(pos);
                });
            },
            error => this.errorMessage = <any>error
        );

        var dataSex = this._SchemeService.GetAllSex().subscribe(
            Sex => {
                this.SexDrop = new Array();
                Sex.forEach(element => {
                    var sex: SexDropdownModel = new SexDropdownModel();
                    sex.ID = element.id;
                    sex.Name = element.name;
                    this.SexDrop.push(sex);
                });
            },
            error => this.errorMessage = <any>error
        );
    }
    async onSubmit(buttonType) {
        if (buttonType === "Submit") {
            if (this.selectedFile !== null) {
                const fd = new FormData();
                fd.append(this.selectedFile.name, this.selectedFile);
                this.EmployeesForms.AvaLink = await this._SchemeService.UploadAvatar(fd).toPromise();
            }
            await this._SchemeService.UpdateScheme(this.EmployeesForms)
                .subscribe(response => {
                    if (response.status == "200") {
                        this.informationService.openSnackBar('Cập nhật nhân viên thành công');
                        this._Route.navigate(['/Employees/All']);
                    }
                    else {
                        this.informationService.openSnackBar('Lỗi xảy ra, vui lòng thử lại');
                    }
                });
        }
    }
    onFileSelected(event) {
        var temp : number = event.target.files.length;
        if (temp !== 0) {
            this.selectedFile = <File>event.target.files[0];
            this.avatarElement = <HTMLImageElement>document.getElementById('Avatar');
            this.avatarElement.src = URL.createObjectURL(event.target.files[0]);
        }
    }
}