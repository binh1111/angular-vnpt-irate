import { ViewEncapsulation, EventEmitter, Input } from '@angular/core';
import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { UserVoteModel } from './Models/app.UserVoteModel';
import { ReportCountingModel } from './Models/app.ReportCountingModel';
import { UserVoteService } from './Services/app.UserVote.Service';
import { ReportService } from './Services/app.Report.Service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ChartDataModel } from './Models/app.ChartDataModel';
import { resolveComponentResources } from '@angular/core/src/metadata/resource_loading';
import { InformationService } from '../information/information.service';
import { DateFilter } from '../AdminDashboard/Models/app.DateFilter';
import * as moment from 'moment';
import { DataSharingService } from '../data-sharing-service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './app.AdminDashboardComponent.html',
    styleUrls: ['../app.component.css', './app.AdminDashboardComponent.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [MatPaginator]
})

export class AdminDashboardComponent implements OnInit {
    AllUserList: UserVoteModel[];
    ReportCounting: ReportCountingModel;
    errorMessage: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    displayedColumns: string[] = ['id', 'name', 'departmentName', 'rateStar', 'ratedCount', 'goodRate', 'badRate'];
    dataSource: any;
    lastChoosenID: number = null;
    fromDate: string = "1970-01-01";
    toDate: string = new Date().toJSON().slice(0, 10);
    moment: any;

    @Output() changeReasonPie: EventEmitter<any> = new EventEmitter();
    reasonData: any;
    subTitle: string;

    @Output() changeEmployeePie: EventEmitter<any> = new EventEmitter();
    empData: any;

    @Output() changeRatePie: EventEmitter<any> = new EventEmitter();
    rateData: any;

    dateFilter: DateFilter;

    constructor(private location: Location, private _Route: Router, private _userVoteService: UserVoteService,
        private _reportService: ReportService, public informationService: InformationService, public dataSharingService: DataSharingService) {
    }

    ngAfterViewInit(){
        if (window.screen.width < 1920){
            var gep = document.getElementById("GEmp") as HTMLParagraphElement;
            gep.textContent = "NV đạt chuẩn";
            var gep = document.getElementById("BEmp") as HTMLParagraphElement;
            gep.textContent = "NV chưa đạt";
            var gep = document.getElementById("GRate") as HTMLParagraphElement;
            gep.textContent = "Hài lòng";
            var gep = document.getElementById("BRate") as HTMLParagraphElement;
            gep.textContent = "Không hài lòng";
        }
    }

    ngOnInit() {
        this.paginator._intl.itemsPerPageLabel = 'Số lượng nhân viên mỗi trang';

        this.dataSharingService.ReportCounting.subscribe(data => this.ReportCounting = data);
        this.dataSharingService.ReasonCounting.subscribe(data => this.reasonData = data);
        this.dataSharingService.DateFilter.subscribe(data => this.dateFilter = data);

        // var counting = await this._reportCountingSerive.GetReportCounting().subscribe(
        //     reportCounting => {
        //         this.ReportCounting = reportCounting;
        //     },
        //     error => this.errorMessage = <any>error
        // );
        // var vote = await this._userVoteService.GetAllUsers().subscribe(
        //     allUsers => {
        //         this.AllUserList = allUsers;
        //         this.dataSource = new MatTableDataSource(this.AllUserList);
        //         this.dataSource.sort = this.sort;
        //         this.dataSource.paginator = this.paginator;
        //     },
        //     error => this.errorMessage = <any>error
        // );
        this.dateFilter = { FromDate: this.fromDate + ' 00:00:00', ToDate: this.toDate + ' 23:59:59'};
        this.fillEmployeeCountingAndTable(this.dateFilter);
        this.fillDefaultData(this.dateFilter);
    }

    async fillEmployeeCountingAndTable(dateFilter: DateFilter) {
        await this._reportService.GetTotalReport(dateFilter).subscribe(response => {
            this.ReportCounting = response.body;

            this.dataSharingService.updateReportCounting(response.body);

            var goodEmpObject: ChartDataModel = {
                field: 'Nhân viên đạt chuẩn',
                count: this.ReportCounting.goodEmployeesCount,
                percentage: Math.round(this.ReportCounting.goodEmployeesCount / this.ReportCounting.employeesCount * 10000) / 100 + '%'
            }

            var badEmpObject: ChartDataModel = {
                field: 'Nhân viên chưa đạt chuẩn',
                count: this.ReportCounting.badEmployeesCount,
                percentage: Math.round(this.ReportCounting.badEmployeesCount / this.ReportCounting.employeesCount * 10000) / 100 + '%'
            }

            var otherCount = this.ReportCounting.employeesCount - this.ReportCounting.goodEmployeesCount - this.ReportCounting.badEmployeesCount;
            var otherEmpObject: ChartDataModel = {
                field: 'Khác',
                count: otherCount,
                percentage: Math.round(otherCount / this.ReportCounting.employeesCount * 10000) / 100 + '%'
            }

            this.empData = [goodEmpObject, badEmpObject, otherEmpObject];
            this.changeEmployeePie.emit(this.empData);

            // this.createRateData(this.ReportCounting);
            // this.changeRatePie.emit(this.rateData);
        })

        await this._userVoteService.GetAllUsers(this.dateFilter).subscribe(response => {
            this.AllUserList = response.body;
            this.dataSource = new MatTableDataSource(this.AllUserList);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        });
    }

    createRateData(reportCounting: ReportCountingModel) {
        var goodRateObject: ChartDataModel = {
            field: 'Đánh giá tốt',
            count: reportCounting.rateGoodCount,
            percentage: Math.round(reportCounting.rateGoodCount / reportCounting.rateCount * 10000) / 100 + '%'
        }

        var badRateObject: ChartDataModel = {
            field: 'Đánh giá xấu',
            count: reportCounting.rateBadCount,
            percentage: Math.round(reportCounting.rateBadCount / reportCounting.rateCount * 10000) / 100 + '%'
        }

        this.rateData = [goodRateObject, badRateObject];
    }

    rowClick(id: number, name: string) {
        if (id == this.lastChoosenID) {
            this.fillDefaultData(this.dateFilter);
            this.lastChoosenID = null;
        }
        else {
            this.fillEmployeeChart(id, name);
            this.lastChoosenID = id;
        }
    }

    async fillDefaultData(dateFilter: DateFilter) {
        this.subTitle = null;

        await this._reportService.GetReasonCounting(dateFilter).subscribe(response => {
            this.reasonData = response.body;
            this.dataSharingService.updateReasonCounting(response.body);
        })

        await this._reportService.GetRateCounting(dateFilter).subscribe(response => {
            this.createRateData(response.body);
        })

        this.changeReasonPie.emit(this.reasonData);
        this.changeReasonPie.emit(this.rateData);
        this.changeReasonPie.emit(this.subTitle);
    }

    async fillEmployeeChart(id: number, name: string) {
        await this._reportService.GetRateCountingById(id, this.dateFilter).subscribe(response => {
            if (response.body.length == 0) {
                this.informationService.openSnackBar("Không có dữ liệu");
                this.lastChoosenID = null;
            }
            else {
                this.createRateData(response.body);
                this._reportService.GetReasonCountingById(id, this.dateFilter).subscribe(response => {
                    this.reasonData = response.body;
                })
                this.subTitle = 'Nhân viên: ' + name;
            }
        })

        this.changeReasonPie.emit(this.reasonData);
        this.changeReasonPie.emit(this.rateData);
        this.changeReasonPie.emit(this.subTitle);

    }
    printPage() {
        window.print();
    }

    resetDate() {
        var tempFromDate = document.getElementById("fromDate") as HTMLInputElement;
        tempFromDate.value = this.fromDate;
        var tempToDate = document.getElementById("toDate") as HTMLInputElement;
        tempToDate.value = this.toDate;
        this.dataSharingService.updateDateFilter({FromDate : this.fromDate, ToDate : this.toDate});
        this.filterData();
    }

    filterData() {
        var tempFromDate = document.getElementById("fromDate") as HTMLInputElement;
        var tempToDate = document.getElementById("toDate") as HTMLInputElement;
        if (moment(tempFromDate.value).isValid() && moment(tempToDate.value).isValid()) {
            this.dateFilter = { FromDate: this.formatDate(tempFromDate.value) + ' 00:00:00', ToDate: this.formatDate(tempToDate.value) + ' 23:59:59'};
            this.dataSharingService.updateDateFilter(this.dateFilter);
            this.fillEmployeeCountingAndTable(this.dateFilter);
            this.fillDefaultData(this.dateFilter);
        }
        else {
            this.informationService.openSnackBar("Ngày không hợp lệ");
        }
    }

    formatDate(date: string) {
        var temp = date.slice(0, 10).split('-');
        return temp[0] + '-' + temp[1] + '-' + temp[2];
    }
}
