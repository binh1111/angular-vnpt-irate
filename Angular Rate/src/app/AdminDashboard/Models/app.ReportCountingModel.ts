export class ReportCountingModel 
{
    public rateCount: number;
    public employeesCount: number;
    public rateGoodCount: number;
    public rateBadCount: number;
    public goodEmployeesCount: number;
    public badEmployeesCount: number;    
}