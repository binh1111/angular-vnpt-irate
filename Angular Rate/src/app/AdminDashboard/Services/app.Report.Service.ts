import { Injectable } from '@angular/core';
import { throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReportCountingModel } from '../Models/app.ReportCountingModel';
import { environment } from '../../../environments/environment';
import { DateFilter } from '../Models/app.DateFilter';
import { DataSharingService } from 'src/app/data-sharing-service';

@Injectable({
    providedIn: 'root'
})

export class ReportService {
    private data: any;
    private apiUrlGetReason = environment.apiEndpoint + "/api/Reason";
    private apiUrlTotalReport = environment.apiEndpoint + "/api/Report";
    private apiUrlGetRateCounting = environment.apiEndpoint + "/api/Report/GetRateCounting";
    private apiUrlReasonCounting = environment.apiEndpoint + "/api/Report/GetReasonCounting";
    private apiUrlGetAllEmployeeReasonCounting = environment.apiEndpoint + "/api/Report/GetAllEmployeeReasonCounting";
    token: any;
    username: any;

    constructor(private http: HttpClient, public dataSharingService: DataSharingService) {
        dataSharingService.Storage.subscribe(data => {
            try{
                this.data = JSON.parse(data.toString());
                this.dataSharingService.updateLoggedIn(true);
            }
            catch{
                
            }
        });
    }

    public GetReason() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.get<any>(this.apiUrlGetReason, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError),
            );
    }

    // Get All Users
    public GetTotalReport(dateFilter: DateFilter) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(this.apiUrlTotalReport, dateFilter, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError),
            );
    }

    public GetRateCounting(dateFilter: DateFilter) {
        const apiUrl = this.apiUrlGetRateCounting;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(apiUrl, dateFilter, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError)
            );
    }

    public GetRateCountingById(id: number, dateFilter: DateFilter) {
        const apiUrl = this.apiUrlGetRateCounting + '/' + id;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(apiUrl, dateFilter, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError)
            );
    }

    public GetReasonCounting(dateFilter: DateFilter) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(this.apiUrlReasonCounting, dateFilter, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError)
            );
    }

    public GetReasonCountingById(id: number, dateFilter: DateFilter) {
        const apiUrl = this.apiUrlReasonCounting + '/' + id;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(apiUrl, dateFilter, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError)
            );
    }

    public GetAllEmployeeReasonCounting(dateFilter: DateFilter) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(this.apiUrlGetAllEmployeeReasonCounting, dateFilter, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };
}
