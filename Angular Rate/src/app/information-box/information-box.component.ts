import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Information } from '../sidebar/information';


@Component({
  selector: 'information-box',
  templateUrl: './information-box.component.html',
  styleUrls: ['./information-box.component.scss']
})
export class InformationBox implements OnInit {
  Information: Information;

  constructor(public dialogRef: MatDialogRef<InformationBox>, @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
    this.Information = this.data;
  }
}