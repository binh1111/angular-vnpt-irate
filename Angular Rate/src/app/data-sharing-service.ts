import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ReportCountingModel } from '../app/AdminDashboard/Models/app.ReportCountingModel';
import { DateFilter } from './AdminDashboard/Models/app.DateFilter';

@Injectable()
export class DataSharingService {
    fromDate: string = "1970-01-01";
    toDate: string = new Date().toJSON().slice(0, 10);

    private reportCounting: ReportCountingModel = { rateCount: 0, employeesCount: 0, rateGoodCount: 0, rateBadCount: 0, goodEmployeesCount: 0, badEmployeesCount: 0 };
    private reasonCounting: Array<any> = [{ field: "test", count: "0", percentage: "0" }];
    private dateFilter: DateFilter = { FromDate: this.fromDate, ToDate: this.toDate };

    private dataSource1 = new BehaviorSubject(this.reportCounting);
    ReportCounting = this.dataSource1.asObservable();

    private dataSource2 = new BehaviorSubject(this.reasonCounting);
    ReasonCounting = this.dataSource2.asObservable();

    private dataSource3 = new BehaviorSubject(this.dateFilter);
    DateFilter = this.dataSource3.asObservable();

    private dataSource4 = new BehaviorSubject(localStorage.getItem('User')
        ? localStorage.getItem('User')
        : {
            username: "string", displayname: "string",
            token: "string", avalink: "string"
        });
    Storage = this.dataSource4.asObservable();
    
    private dataSource5 = new BehaviorSubject(false);
    LoggedIn = this.dataSource5.asObservable();

    constructor() { }

    updateReportCounting(newReportCounting: any) {
        this.dataSource1.next(newReportCounting);
    }

    updateReasonCounting(newReasonCounting: any) {
        this.dataSource2.next(newReasonCounting);
    }

    updateDateFilter(newDateFilter: any) {
        this.dataSource3.next(newDateFilter);
    }

    updateStorage(newStorage: any) {
        this.dataSource4.next(newStorage);
    }

    updateLoggedIn(value: boolean) {
        this.dataSource5.next(value);
    }
}