import { Component } from '@angular/core';
import { DataSharingService } from './data-sharing-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./Content/vendor/bootstrap/css/bootstrap.min.css',
              './Content/vendor/metisMenu/metisMenu.min.css',
              './Content/dist/css/sb-admin-2.css',
              './Content/vendor/font-awesome/css/font-awesome.min.css'
            ]
})
export class AppComponent {
  title = 'Đánh giá cán bộ công chức';
  loggedIn : boolean = false;
  
  constructor(public dataSharingService : DataSharingService) {
    dataSharingService.LoggedIn.subscribe(data => this.loggedIn = data);
  }
}
