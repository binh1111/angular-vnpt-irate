import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ReportService } from '../AdminDashboard/Services/app.Report.Service';
import { ChartDataModel } from '../AdminDashboard/Models/app.ChartDataModel';
import { ReportCountingModel } from '../AdminDashboard/Models/app.ReportCountingModel';
import { element } from 'protractor';
import { OverlayKeyboardDispatcher } from '@angular/cdk/overlay';
import { DateFilter } from '../AdminDashboard/Models/app.DateFilter';
import { DataSharingService } from '../data-sharing-service';
import { RateService } from '../RateComponent/Services/app.Rate.Service';
import { Filter } from '../RateComponent/Models/app.Filter';

@Component({
  selector: 'app-print-page',
  templateUrl: './print-page.component.html',
  styleUrls: ['./print-page.component.scss']
})
export class PrintPageComponent implements OnInit {
  ReportCounting: ReportCountingModel;
  displayedColumnsTable1: string[] = ['field', 'count', 'percentage'];
  displayedColumnsTable2: string[];
  displayedColumnsTable3: string[] = ['id', 'employeeName', 'score', 'phoneNumber', 'sendDate'];

  columns: Array<any> = [
    { name: 'name', label: 'Họ và tên', value: 'name' },
    { name: 'fieldName', label: 'Lĩnh vực', value: 'fieldName' },
    { name: 'rateStar', label: 'Đánh giá', value: 'rateStar' },
    { name: 'rateCount', label: 'Số lượt đánh giá', value: 'rateCount' },
    { name: 'goodRate', label: 'Đánh giá tốt', value: 'goodRate' },
    { name: 'badRate', label: 'Đánh giá xấu', value: 'badRate' }
  ]

  additionalColumns: string[] = ['0'];

  totalReasonCount: number;
  employeeCount: number;
  rateCount: number;
  employeeReportTable: any;
  reasonList: any;
  @Output() changeEmployeePie: EventEmitter<any> = new EventEmitter();
  empData: any;

  @Output() changeRatePie: EventEmitter<any> = new EventEmitter();
  rateData: any;

  @Output() changeReasonPie: EventEmitter<any> = new EventEmitter();
  reasonData: any;
  rateList: any;

  fromDate: string = "1970-01-01";
  toDate: string = new Date().toJSON().slice(0, 10);
  dateFilter: DateFilter = { FromDate: this.fromDate, ToDate: this.toDate };

  constructor(private _reportService: ReportService, public _dataSharingService: DataSharingService, public _rateService: RateService) { }

  async ngOnInit() {
    this._dataSharingService.ReportCounting.subscribe(data => {
      this.ReportCounting = data;

      var goodEmpObject: ChartDataModel = {
        field: 'Nhân viên đạt chuẩn',
        count: this.ReportCounting.goodEmployeesCount,
        percentage: Math.round(this.ReportCounting.goodEmployeesCount / this.ReportCounting.employeesCount * 10000) / 100 + '%'
      }

      var badEmpObject: ChartDataModel = {
        field: 'Nhân viên chưa đạt chuẩn',
        count: this.ReportCounting.badEmployeesCount,
        percentage: Math.round(this.ReportCounting.badEmployeesCount / this.ReportCounting.employeesCount * 10000) / 100 + '%'
      }

      var otherCount = this.ReportCounting.employeesCount - this.ReportCounting.goodEmployeesCount - this.ReportCounting.badEmployeesCount;
      var otherEmpObject: ChartDataModel = {
        field: 'Khác',
        count: otherCount,
        percentage: Math.round(otherCount / this.ReportCounting.employeesCount * 10000) / 100 + '%'
      }

      this.empData = [goodEmpObject, badEmpObject, otherEmpObject];
      this.changeEmployeePie.emit(this.empData);
      this.employeeCount = this.ReportCounting.employeesCount;

      this.createRateData(this.ReportCounting);
      this.changeRatePie.emit(this.rateData);
      this.rateCount = this.rateData.reduce((total, item) => total += Number(item.count), 0);

    });

    this._dataSharingService.ReasonCounting.subscribe(data => {
      this.reasonData = data;
      this.totalReasonCount = this.reasonData.reduce((total: number, item) => total += Number(item.count), 0);
    })

    this._dataSharingService.DateFilter.subscribe(data => {
      this.dateFilter = data;
      this._reportService.GetAllEmployeeReasonCounting(this.dateFilter).subscribe(response => {
        const temp = response.body.map(x => x.reasonCount);
        const x = Object.assign(this.additionalColumns, temp);
        let table: any = [];
        response.body.forEach(element => {
          var i = response.body.indexOf(element);
          var t = response.body[i];
          var y = Object.assign(t, x[i]);
          table.push(y);
        });
        this.employeeReportTable = table;
      });
    })

    await this._reportService.GetReason().subscribe(response => {
      this.reasonList = response.body;
      this.reasonList.forEach(element => {
        const str = 'reason' + this.reasonList.indexOf(element);
        let temp: any = { name: str, label: element.text, value: this.reasonList.indexOf(element) };
        this.columns.push(temp);
        this.additionalColumns.push(str);
        this.displayedColumnsTable2 = this.columns.map(column => column.name);
      });
    })
    var filter: Filter = { FromDate: this.dateFilter.FromDate, ToDate: this.dateFilter.ToDate, Type: 0 };
    await this._rateService.GetAllRates(filter).subscribe(response => {
      this.rateList = response.body;
    })
  }

  createRateData(reportCounting: ReportCountingModel) {
    var goodRateObject: ChartDataModel = {
      field: 'Đánh giá tốt',
      count: reportCounting.rateGoodCount,
      percentage: Math.round(reportCounting.rateGoodCount / reportCounting.rateCount * 10000) / 100 + '%'
    }

    var badRateObject: ChartDataModel = {
      field: 'Đánh giá xấu',
      count: reportCounting.rateBadCount,
      percentage: Math.round(reportCounting.rateBadCount / reportCounting.rateCount * 10000) / 100 + '%'
    }

    this.rateData = [goodRateObject, badRateObject];
  }

  qualify(value) {
    if (value == 0)
      return "Không hài lòng";
    else
      return "Hài lòng";
  }
}
