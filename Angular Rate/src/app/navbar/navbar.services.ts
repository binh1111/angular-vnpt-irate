import { Injectable } from '@angular/core';
import { throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { DataSharingService } from '../data-sharing-service';

@Injectable({
    providedIn: 'root'
})

export class NotificaionService {
    private data: any;
    private apiGetUnreadRate  = environment.apiEndpoint + "/api/Rate/GetUnreadRate";
    private apiMarkAllAsRead = environment.apiEndpoint + "/api/Rate/MarkAllAsRead";
    token: any;
    username: any;

    constructor(private http: HttpClient, public dataSharingService: DataSharingService) {
        dataSharingService.Storage.subscribe(data => {
            try{
                this.data = JSON.parse(data.toString());
            }
            catch{
                
            }
        });
        this.token = this.data.token;
    }

    public GetUnreadRate() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<any>(this.apiGetUnreadRate, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError),
            );
    }

    public MaskAllAsRead() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<any>(this.apiMarkAllAsRead, { headers: headers, observe: 'response' })
            .pipe(tap(data => data),
                catchError(this.handleError),
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };
}
