import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { NotificaionService } from './navbar.services';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [NgbDropdownConfig]
})

export class NavbarComponent implements OnInit {
  public sidebarOpened = false;
  displayName: string;
  avaLink: string;
  isDestroyed: boolean = false;
  unreadRateCount: number = 0;
  moment: any;
  // toggleOffcanvas() {
  //   this.sidebarOpened = !this.sidebarOpened;
  //   if (this.sidebarOpened) {
  //     document.querySelector('.sidebar-offcanvas').classList.add('active');
  //   }
  //   else {
  //     document.querySelector('.sidebar-offcanvas').classList.remove('active');
  //   }
  // }
  constructor(config: NgbDropdownConfig, private _notificationService: NotificaionService) {
    config.placement = 'bottom-right';
  }
  ngOnInit() {
    this.displayName = JSON.parse(localStorage.getItem('User')).displayname;
    this.avaLink = environment.apiEndpoint + JSON.parse(localStorage.getItem('User')).avalink;

    if (this.avaLink != null) {
      var temp: HTMLImageElement = <HTMLImageElement>document.getElementById('avatar-nav-bar');
      temp.src = this.avaLink;
    }
  }
  ngOnDestroy() {
    this.isDestroyed = true;
  }

  ngAfterViewInit() {
    this.poll();
  }

  async poll() {
    let i: number = 1;
    while (!this.isDestroyed) {
      // code to poll server and update models and view ...
      await this._notificationService.GetUnreadRate().subscribe(response => {
        this.unreadRateCount = response.body.length;
        var panel = document.getElementById("flexNotificationBox");
        panel.innerHTML = "";

        response.body.forEach(element => {
          panel.appendChild(this.generateNotificationTag(element.employeeName, element.phone, element.time, element.id));
        });

      })
      await this.sleep(15000);
    }
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  generateNotificationTag(employeeName: string, phone: string, time: string, id: string): HTMLElement {
    var a = document.createElement('span');
    a.className = "dropdown-item preview-item";

    var div = document.createElement('div');
    div.className = "preview-item-content";
    var h6 = document.createElement('h6');
    h6.textContent = "Nhân viên " + employeeName;
    h6.className = "preview-subject font-weight-medium";
    div.appendChild(h6);

    var p1 = document.createElement('p');
    p1.className = "font-weight-light small-text";
    p1.textContent = "Thời gian: " + moment(time).format('DD/MM/YYYY, h:mm');
    div.appendChild(p1);

    var p2 = document.createElement('p');
    p2.className = "font-weight-light small-text";
    p2.textContent = "Người đánh giá: " + phone;
    div.appendChild(p2);

    a.appendChild(div);

    var divider = document.createElement('div');
    divider.className = "dropdown-divider";
    a.appendChild(divider);
    return a;
  }

  async MarkAllAsRead() {
    await this._notificationService.MaskAllAsRead().subscribe();
  }
  toggled(event) {
    if (!event) {
      this.MarkAllAsRead();
      this.unreadRateCount = 0;
      var panel = document.getElementById("flexNotificationBox");
      panel.innerHTML = "";
    }
  }

  // generateNotificationTag(employeeName : string, phone : string, time : string, id : string) : HTMLElement{
  //   var a = document.createElement('span');
  //   var text = "Nhân viên " + employeeName + " bị đánh giá xấu";
  //   var linkText = document.createTextNode(text);
  //   a.appendChild(linkText);
  //   a.className = "dropdown-item preview-item";
  //   var div1 = document.createElement('div');
  //   div1.className = "preview-item-content";
  //   var h6 = document.createElement('h6');
  //   h6.className = "preview-subject font-weight-medium";
  //   div1.appendChild(h6);
  //   var p = document.createElement('p');
  //   p.className = "font-weight-light small-text";
  //   p.textContent = "Thời gian: " + time;
  //   div1.appendChild(p);
  //   a.appendChild(div1);
  //   var divider = document.createElement('div');
  //   divider.className = "dropdown-divider";
  //   a.appendChild(divider);
  //   return a;
  // }
}
