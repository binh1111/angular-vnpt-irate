import {MatSnackBar} from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { InformationComponent } from './information.component';

@Injectable()
export class InformationService {

    durationInSeconds =3;
    message:string;
    constructor(private _snackBar: MatSnackBar) {
    }
  
    openSnackBar(message:string) {
      this.message = message;
      const snackBar=  this._snackBar.openFromComponent(InformationComponent, {
        duration: this.durationInSeconds * 1000,
      });
      snackBar.instance.message = message;
    }
}
