import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { BsDatepickerModule, } from 'ngx-bootstrap/datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatAutocomplete, MatAutocompleteModule } from '@angular/material/autocomplete';
import { AppComponent } from './app.component';
import { AllSchemeComponent } from './SchemeMasters/app.AllScheme.Component';
import { EditSchemeComponent } from './SchemeMasters/app.EditScheme.Component';
import { MatSortModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, MatSnackBar, MatSnackBarConfig, MatSnackBarModule, MatButtonModule } from '@angular/material';
import { LoginComponent } from './Login/app.LoginComponent';
import { AppAdminLayoutComponent } from './_layout/app-adminlayout.component';
import { AdminDashboardComponent } from './AdminDashboard/app.AdminDashboardComponent';
import { AppUserLayoutComponent } from './_layout/app-userlayout.component';
import { AdminLogoutComponent } from './Login/app.AdminLogout.Component';
import { UserLogoutComponent } from './Login/app.UserLogout.Component';
import { AdminAuthGuardService } from './AuthGuard/AdminAuthGuardService';
import { UserAuthGuardService } from './AuthGuard/UserAuthGuardService';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RatingModule } from 'primeng/rating';
import { enableProdMode } from '@angular/core';
import { ChartAllModule, AccumulationChartAllModule, RangeNavigatorAllModule } from '@syncfusion/ej2-angular-charts';
import {
  PieSeriesService, AccumulationLegendService, AccumulationTooltipService, AccumulationAnnotationService,
  AccumulationDataLabelService
} from '@syncfusion/ej2-angular-charts';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { DialogService } from './dialog-box/dialog-box.Service';
import { AddSchemeComponent } from './SchemeMasters/app.AddSchemeComponent';
import { ReasonChartComponent } from './reasonChart/reasonChart.component';
import { EmployeeChartComponent } from './employeeChart/employeeChart.component';
import { RateChartComponent } from './rateChart/rateChart.component';
import { InformationService } from './information/information.service';
import { InformationComponent } from './information/information.component';
import { PrintPageComponent } from './print-page/print-page.component';
import { EmployeeChartPrintComponent } from './employeeChart-print/employeeChart-print.component';
import { RateChartPrintComponent } from './rateChart-print/rateChart-print.component';
import { ReasonChartPrintComponent } from './reasonChart-print/reasonChart-print.component';
import * as moment from 'moment';
import {DataSharingService} from '../app/data-sharing-service';
import {MatDialogModule} from '@angular/material';
import {InformationBox} from '../app/information-box/information-box.component';
import {NotificaionService} from '../app/navbar/navbar.services';
import {MatTableExporterModule} from 'mat-table-exporter';
import { NgxSelectModule } from 'ngx-select-ex';
import { AllRatesComponent } from './RateComponent/app.AllRates.Component';

enableProdMode();
@NgModule({
  declarations: [
    InformationBox,
    AppComponent,
    AppAdminLayoutComponent,
    AppUserLayoutComponent,
    AllSchemeComponent,
    EditSchemeComponent,
    AddSchemeComponent,
    LoginComponent,
    AdminLogoutComponent,
    UserLogoutComponent,
    AdminDashboardComponent,
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    DialogBoxComponent,
    ReasonChartComponent,
    EmployeeChartComponent,
    RateChartComponent,
    InformationComponent,
    PrintPageComponent,
    EmployeeChartPrintComponent,
    RateChartPrintComponent,
    ReasonChartPrintComponent,
    AllRatesComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ChartAllModule, AccumulationChartAllModule, RangeNavigatorAllModule,
    // BsDatepickerModule.forRoot(),
    MatTableModule,
    NgbModule.forRoot(),
    MatAutocompleteModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    RatingModule,
    MatDialogModule,
    MatButtonModule,
    MatTableExporterModule,
    NgxSelectModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: 'Employees',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Add', component: AddSchemeComponent, canActivate: [AdminAuthGuardService] },
          { path: 'Edit/:employeesId', component: EditSchemeComponent, canActivate: [AdminAuthGuardService] },
          { path: 'All', component: AllSchemeComponent, canActivate: [AdminAuthGuardService] }
        ]
      },
      {
        path: 'Admin',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Dashboard', component: AdminDashboardComponent, canActivate: [AdminAuthGuardService] }
        ]
      },
      {
        path: 'Rate',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'All', component: AllRatesComponent, canActivate: [AdminAuthGuardService] }
        ]
      },
      { path: 'Login', component: LoginComponent },
      { path: 'AdminLogout', component: AdminLogoutComponent },
      { path: 'UserLogout', component: UserLogoutComponent },

      { path: '', redirectTo: "Login", pathMatch: 'full' },
      { path: '**', redirectTo: "Login", pathMatch: 'full' },
    ], { useHash: false })
  ],
  // exports: [BsDatepickerModule],
  providers: [DatePipe, AdminAuthGuardService, UserAuthGuardService, DialogService, PieSeriesService, AccumulationLegendService, AccumulationTooltipService, AccumulationDataLabelService,
    AccumulationAnnotationService, InformationService, DataSharingService, NotificaionService],
  bootstrap: [AppComponent],
  entryComponents: [DialogBoxComponent, InformationComponent, InformationBox],
})
export class AppModule { }
