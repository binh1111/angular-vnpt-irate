import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from './Models/app.LoginModel';
import { LoginService } from './Services/app.LoginService';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { InformationService } from '../information/information.service';

@Component({
    templateUrl: './app.login.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css'
    ]
})

export class LoginComponent implements OnInit {

    ngOnInit(): void {
        localStorage.clear();
    }
    private _loginservice;
    output: any;

    actionButtonLabel: string = 'Retry';
    action: boolean = false;
    setAutoHide: boolean = true;
    autoHide: number = 2000;
    verticalPosition: MatSnackBarVerticalPosition = 'bottom';
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';


    constructor(private _Route: Router, public snackBar: MatSnackBar, loginservice: LoginService, public info: InformationService) {
        this._loginservice = loginservice;
    }

    LoginModel: LoginModel = new LoginModel();

    onSubmit() {
        this._loginservice.validateLoginUser(this.LoginModel).subscribe(
            response => {
                if (response.token == null && response.usertype == "0") {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;

                    //this.snackBar.open("Sai Mật khẩu hoặc Tên đăng nhập", this.action ? this.actionButtonLabel : undefined, config);
                    this.info.openSnackBar("Sai Mật khẩu hoặc Tên đăng nhập");
                    this._Route.navigate(['Login']);
                }

                if (response.usertype == "1") {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;

                    //this.snackBar.open("Đăng nhập thành công với quyền admin", this.action ? this.actionButtonLabel : undefined, config);
                    this.info.openSnackBar("Đăng nhập thành công với quyền admin");

                    this._Route.navigate(['/Admin/Dashboard']);
                }

                if (response.usertype == "2") {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;

                    //this.snackBar.open("Đăng nhập thành công", this.action ? this.actionButtonLabel : undefined, config);
                    this.info.openSnackBar("Đăng nhập thành côn với quyền admin");
                    this._Route.navigate(['/User/Dashboard']);
                }
            });

    }
}