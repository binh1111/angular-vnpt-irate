export class Information{
    public Id : number;
    public Username : string;
    public Name : string;
    public Sex : string;
    public Field : string;
    public Position : string;
    public AvatarLink : string;
}