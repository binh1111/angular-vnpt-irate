import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { InformationBox } from '../information-box/information-box.component';
import { Information } from './information';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DataSharingService } from '../data-sharing-service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  public samplePagesCollapsed = true;
  data: any;
  Information: Information;
  displayName : any;

  apiGetInfor: string = environment.apiEndpoint + "/api/Employees/GetInformationByUsername/";

  constructor(public dialog: MatDialog, public http: HttpClient, public dataSharingService : DataSharingService) {
    dataSharingService.Storage.subscribe(data => {
      try{
          this.data = JSON.parse(data.toString());
      }
      catch{
          
      }
  });
    this.displayName = this.data.displayname;
  }

  ngAfterViewInit() {
    if (this.data.avalink != null) {
      var temp: HTMLImageElement = <HTMLImageElement>document.getElementById('avatar-side-bar');
      temp.src = environment.apiEndpoint + this.data.avalink;
    }
  }
  async informationButtonClick() {
    await this.GetInfor().subscribe(response => {
      this.Information = response.body;
      var x = '25%';
      var y = '40%';
      if (window.screen.width < 1920){
        x = '480px';
        y = '375px';
      }
      const dialogRef = this.dialog.open(InformationBox, {
        width: x,
        height: y,
        data: this.Information
      });
    })
  }
  public GetInfor() {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
    return this.http.get<Information>(this.apiGetInfor + this.data.username, { headers: headers, observe: 'response' }).pipe(tap(data => data)
    );
  }
}
