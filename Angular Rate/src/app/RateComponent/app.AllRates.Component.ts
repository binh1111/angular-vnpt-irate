import { Component, OnInit, ViewChild } from '@angular/core';
import { RateService } from './Services/app.Rate.Service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DialogService } from '../dialog-box/dialog-box.Service';
import { InformationService } from "../information/information.service";
import * as moment from 'moment';
import { Filter } from './Models/app.Filter';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormControl } from '@angular/forms';
import { MatTableExporterModule } from 'mat-table-exporter';

@Component({
  templateUrl: './app.AllRates.Component.html',
  styleUrls: ['./app.AllRates.Component.css', '../Content/vendor/bootstrap/css/bootstrap.min.css', '../Content/vendor/font-awesome/css/font-awesome.min.css']

})

export class AllRatesComponent implements OnInit {
  private _rateService;
  RateData: any;
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'employeeName', 'score', 'phoneNumber', 'sendDate'];
  dataSource: any;
  response: any;
  fromDate: string = "1970-01-01";
  toDate: string = new Date().toJSON().slice(0, 10);
  moment: any;
  comboboxFilterData: Object[] =
    [{ id: 0, text: "Tất cả loại đánh giá" },
    { id: 1, text: "Đánh giá tốt" },
    { id: 2, text: "Đánh giá xấu" }];
  filter: Filter;
  public qualityControl = new FormControl();

  constructor(private location: Location, private _Route: Router, private rateService: RateService,
    public informationService: InformationService) {
    this._rateService = rateService;
  }

  ngOnInit() {
    this.paginator._intl.itemsPerPageLabel = 'Số lượng đánh giá mỗi trang';
    this.filter = { FromDate: this.fromDate + ' 00:00:00', ToDate: this.toDate + ' 23:59:59', Type: 0 };
    this.reloadTable();
  }
  filterData() {
    var tempFromDate = document.getElementById("fromDate") as HTMLInputElement;
    var tempToDate = document.getElementById("toDate") as HTMLInputElement;
    if (moment(tempFromDate.value).isValid() && moment(tempToDate.value).isValid()) {
      this.filter = { FromDate: this.formatDate(tempFromDate.value) + ' 00:00:00', ToDate: this.formatDate(tempToDate.value) + ' 23:59:59', Type: this.qualityControl.value };
      this.reloadTable();
    }
    else {
      this.informationService.openSnackBar("Ngày không hợp lệ");
    }
  }
  reloadTable() {
    this._rateService.GetAllRates(this.filter).subscribe(
      response => {
        this.RateData = response.body;
        this.dataSource = new MatTableDataSource(this.RateData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error => this.errorMessage = <any>error
    );
  }

  formatDate(date: string) {
    var temp = date.slice(0, 10).split('-');
    return temp[0] + '-' + temp[1] + '-' + temp[2];
  }

  resetDate() {
    var tempFromDate = document.getElementById("fromDate") as HTMLInputElement;
    tempFromDate.value = this.fromDate;
    var tempToDate = document.getElementById("toDate") as HTMLInputElement;
    tempToDate.value = this.toDate;
    var tempQualityFilter = document.getElementById("qualityFilter") as NgxSelectModule;
    this.qualityControl.setValue(0);
    this.filterData();
  }

  qualify(value) {
    if (value == 0)
      return "Không hài lòng";
    else
      return "Hài lòng";
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}