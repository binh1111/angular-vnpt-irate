import { Injectable } from "@angular/core";
import { Observable, throwError, observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
// import { environment } from "src/app/Shared/environment";
import { environment } from '../../../environments/environment';
import { InformationService } from "../../information/information.service";
import { Filter } from "../Models/app.Filter";
import { DataSharingService } from "src/app/data-sharing-service";

@Injectable({
    providedIn: 'root'
})

export class RateService {
    private data: any;
    private apiUrl = environment.apiEndpoint + "/api/Rate/GetAllRateWithName";

    constructor(private http: HttpClient, public informationService: InformationService, public dataSharingService: DataSharingService) {
        dataSharingService.Storage.subscribe(data => {
            try{
                this.data = JSON.parse(data.toString());
            }
            catch{
                
            }
        });
    }

    // Save Scheme
    public GetAllRates(filter: Filter) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.data.token}`);
        return this.http.post<any>(this.apiUrl, filter, { headers: headers, observe: 'response' })
            .pipe(
                catchError(this.handleError)
            );

    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
            if (error.status === 400) {
                this.informationService.openSnackBar("Đã có lỗi xảy ra");
            }
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };



}
